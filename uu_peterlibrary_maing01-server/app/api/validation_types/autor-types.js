/* eslint-disable */
const AutorCreateDtoInType = shape({
  name: string(40).isRequired(),
  born: string().isRequired(),
  nationality: string(20).isRequired(),
})

const AutorUpdateDtoInType = shape({
  id: id().isRequired(),
  name: string(40),
  born: string(),
  nationality: string(20),
});

const AutorGetDtoInType = shape({
  id: id().isRequired(),
});

const AutorDeleteDtoInType = shape({
  id: id().isRequired(),
});

const AutorListDtoInType = shape({
  id: id(),
  name: string(40),
  born: string(),
  nationality: string(20),
});
