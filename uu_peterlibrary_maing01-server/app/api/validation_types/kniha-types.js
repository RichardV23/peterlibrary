/* eslint-disable */
const knihaCreateDtoInType = shape({
  name: string(50).isRequired(),
  description: string(300).isRequired(),
  pages: integer().isRequired(),
  language: string(20).isRequired(),
  autorId: string().isRequired(),
});

const knihaGetDtoInType = shape({
  id: id().isRequired(),
});

const knihaUpdateDtoInType = shape({
  id: id().isRequired(),
  name: string(50),
  description: string(300),
  pages: integer(),
  language: string(20),
  autorId: string(20),
});

const knihaDeleteDtoInType = shape({
  id: id().isRequired(),
});

const knihaListDtoInType = shape({
  id: id(),
  name: string(50),
  description: string(300),
  pages: integer(),
  language: string(20),
  autorId: string(),
});
