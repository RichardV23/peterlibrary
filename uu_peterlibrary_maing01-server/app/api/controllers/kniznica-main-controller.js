"use strict";
const KniznicaMainAbl = require("../../abl/kniznica-main-abl");

class KniznicaMainController {
  init(ucEnv) {
    return KniznicaMainAbl.init(ucEnv.getUri(), ucEnv.getDtoIn(), ucEnv.getSession());
  }

  load(ucEnv) {
    return KniznicaMainAbl.load(ucEnv.getUri(), ucEnv.getSession());
  }

  loadBasicData(ucEnv) {
    return KniznicaMainAbl.loadBasicData(ucEnv.getUri(), ucEnv.getSession());
  }
}

module.exports = new KniznicaMainController();
