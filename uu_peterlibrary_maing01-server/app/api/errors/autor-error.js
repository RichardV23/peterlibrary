"use strict";

const KniznicaMainUseCaseError = require("./kniznica-main-use-case-error.js");
const AUTOR_ERROR_PREFIX = `${KniznicaMainUseCaseError.ERROR_PREFIX}autor/`;

const Create = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}create/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  AutorCreateFailed: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}AutorCreateFailed`;
      this.message = "Creating autor by DAO method create failed";
    }
  },
};

const Update = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}update/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  AutorDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}AutorDoesNotExist`;
      this.message = "Autor does not exist";
    }
  },

  AutorUpdateFailed: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}AutorUpdateFailed`;
      this.message = "Updating autor by DAO method update failed";
    }
  },
};

const Get = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}get/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  AutorDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}AutorDoesNotExist`;
      this.message = "Autor does not exist";
    }
  },
};

const Delete = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}delete/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  AutorDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}AutorDoesNotExist`;
      this.message = "Autor does not exist";
    }
  },

  AutorDeleteFailed: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}AutorDeleteFailed`;
      this.message = "Deleting autor by DAO method delete failed";
    }
  },

  AutorCannotDelete: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}AutorDeleteFailed`;
      this.message = "Deleting autor by DAO method delete failed, assignment";
    }
  },
};

const List = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}list/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  AutorDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}AutorDoesNotExist`;
      this.message = "Autor does not exist";
    }
  },
};

module.exports = {
  Create,
  Update,
  Get,
  Delete,
  List,
};
