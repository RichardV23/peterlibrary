"use strict";

const KniznicaMainUseCaseError = require("./kniznica-main-use-case-error.js");
const KNIHA_ERROR_PREFIX = `${KniznicaMainUseCaseError.ERROR_PREFIX}kniha/`;

const Create = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}create/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },
  KnihaCreateFailed: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}knihaCreateFailed`;
      this.message = "Creating kniha by DAO method create failed";
    }
  },
};

const Update = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}update/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  KnihaDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}KnihaDoesNotExist`;
      this.message = "Kniha does not exist";
    }
  },

  KnihaUpdateFailed: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}KnihaUpdateFailed`;
      this.message = "Updating kniha by DAO method update failed";
    }
  },
};

const Get = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}get/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  KnihaDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}KnihaDoesNotExist`;
      this.message = "Kniha does not exist";
    }
  },
};

const Delete = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}delete/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  KnihaDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}KnihaDoesNotExist`;
      this.message = "Kniha does not exist";
    }
  },

  KnihaDeleteFailed: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}KnihaDeleteFailed`;
      this.message = "Deleting kniha by DAO method delete failed";
    }
  },
};

const List = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}list/`,

  InvalidDtoIn: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}invalidDtoInType`; /* Create in video  */
      this.message = "Invalid input data";
    }
  },

  KnihaDoesNotExist: class extends KniznicaMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}KnihaDoesNotExist`;
      this.message = "Kniha does not exist";
    }
  },
};

module.exports = {
  Create,
  Get,
  Update,
  Delete,
  List,
};
