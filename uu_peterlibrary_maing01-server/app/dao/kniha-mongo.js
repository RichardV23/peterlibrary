"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;

class KnihaMongo extends UuObjectDao {
  async createSchema() {}
  async create(kniha) {
    return await super.insertOne(kniha);
  }

  async get(awid, id) {
    let filter = {
      awid: awid,
      id: id,
    };
    return await super.findOne(filter);
  }

  async update(uuObject) {
    let filter = {
      awid: uuObject.awid,
      id: uuObject.id,
    };
    return await super.findOneAndUpdate(filter, uuObject, "NONE");
  }

  async delete(uuObject) {
    let filter = {
      awid: uuObject.awid,
      id: uuObject.id,
    };
    return await super.deleteOne(filter);
  }

  async getCountByAutorId(uuObject) {
    let filter = {
      awid: uuObject.awid,
      autorId: uuObject.id,
    };
    let books = await super.find(filter)

    return books.itemList.length;
  }

  async list(awid) {
    let filter = {
      awid: awid,
    };
    return await super.find(filter);
  }
}

module.exports = KnihaMongo;
