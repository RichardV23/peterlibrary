"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/kniha-error.js");

const WARNINGS = {
  unsupportedKeys: {
    CODE: `${Errors.Create.UC_CODE}.unsupportedKeys`,
  },
};

class KnihaAbl {
  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("kniha");
  }

  async create(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("knihaCreateDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Create.InvalidDtoIn
    );

    let knihaResult;

    try {
      knihaResult = await this.dao.create({...dtoIn, awid});
    } catch (e) {
      throw new Errors.Create.KnihaCreateFailed({ uuAppErrorMap }, e);
    }
    return {
      ...knihaResult,
      uuAppErrorMap,
    };
  }

  async update(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("knihaUpdateDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Update.InvalidDtoIn
    );

    let kniha = await this.dao.get(awid, dtoIn.id);

    if (!kniha) {
      throw new Errors.Update.KnihaDoesNotExist({ uuAppErrorMap }, { knihaId: dtoIn.id });
    }

    let knihaResult;

    try {
      knihaResult = await this.dao.update({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Update.KnihaUpdateFailed(uuAppErrorMap, { uuAppErrorMap });
    }
    return {

      ...knihaResult,
      uuAppErrorMap,
    };
  }

  async get(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("knihaGetDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    let kniha = await this.dao.get(awid, dtoIn.id);

    if (!kniha) {
      throw (new Errors.Get.KnihaDoesNotExist(uuAppErrorMap), { knihaId: dtoIn.id });
    }
    return {
      ...kniha,
      uuAppErrorMap,
    };
  }

  async delete(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("knihaDeleteDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Delete.InvalidDtoIn
    );

    let autor = await this.dao.get(awid, dtoIn.id);

    if (!autor) {
      throw (new Errors.Delete.KnihaDoesNotExist(uuAppErrorMap), { knihaId: dtoIn.id });
    }

    try {
      await this.dao.delete({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Delete.KnihaDeleteFailed(uuAppErrorMap, { uuAppErrorMap });
    }
    return {
      uuAppErrorMap,
    };
  }

  async list(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("knihaListDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.List.InvalidDtoIn
    );

    let kniha = await this.dao.list(awid, dtoIn);

    if (!kniha) {
      throw (new Errors.List.KnihaDoesNotExist(uuAppErrorMap), {...dtoIn, awid});
    }
    return {
      ...kniha,
      uuAppErrorMap,
    };
  }
}
module.exports = new KnihaAbl();
