"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/autor-error.js");

const WARNING = {
  unsupportedKeys: {
    CODE: `${Errors.Create.UC_CODE}.unsupportedKeys`,
  },
};

class AutorAbl {
  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("autor");
    this.bookDao = DaoFactory.getDao("kniha");
  }

  async create(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("AutorCreateDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNING.unsupportedKeys.CODE,
      Errors.Create.InvalidDtoIn
    );

    let autorResult;

    try {
      autorResult = await this.dao.create({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Create.AutorCreateFailed({ uuAppErrorMap }, e);
    }
    return {
      ...autorResult,
      uuAppErrorMap,
    };
  }

  async update(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("AutorUpdateDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNING.unsupportedKeys.CODE,
      Errors.Update.InvalidDtoIn
    );

    let autor = await this.dao.get(awid, dtoIn.id);

    if (!autor) {
      throw new Errors.Update.AutorDoesNotExist({ uuAppErrorMap }, { autorId: dtoIn.id });
    }

    let autorResult;

    try {
      autorResult = await this.dao.update({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Update.AutorUpdateFailed(uuAppErrorMap, { uuAppErrorMap });
    }
    return {
      ...autorResult,
      uuAppErrorMap,
    };
  }

  async get(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("AutorGetDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNING.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    let autor = await this.dao.get(awid, dtoIn.id);

    if (!autor) {
      throw (new Errors.Get.AutorDoesNotExist(uuAppErrorMap), { autorId: dtoIn.id });
    }
    return {
      ...autor,
      uuAppErrorMap,
    };
  }

  async delete(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("AutorDeleteDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNING.unsupportedKeys.CODE,
      Errors.Delete.InvalidDtoIn
    );

    let autor = await this.dao.get(awid, dtoIn.id);

    if (!autor) {
      throw new Errors.Delete.AutorDoesNotExist({uuAppErrorMap}, { autorId: dtoIn.id });
    }

    let bookCount = await this.bookDao.getCountByAutorId({ awid, id: dtoIn.id });

    //make a new error for that command and replace it
    if (bookCount > 0) {
      throw new Errors.Delete.AutorCannotDelete({uuAppErrorMap}, { autorId: dtoIn.id });
    }

    try {
      await this.dao.delete({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Delete.AutorDeleteFailed(uuAppErrorMap, { uuAppErrorMap });
    }
    return {
      uuAppErrorMap,
    };
  }

  async list(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("AutorListDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNING.unsupportedKeys.CODE,
      Errors.List.InvalidDtoIn
    );

    let autor = await this.dao.list(awid, dtoIn);

    if (!autor) {
      throw (new Errors.List.AutorDoesNotExist(uuAppErrorMap), { ...dtoIn, awid });
    }
    return {
      ...autor,
      uuAppErrorMap,
    };
  }
}

module.exports = new AutorAbl();
